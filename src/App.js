import './App.css';
import Header from './App/header/Header';
import Home from './App/home/Home';
import { BrowserRouter as Router, Switch, Route } from "react-router-dom" 
import Checkout from './App/checkout/Checkout';
import Login from './App/auth/Login';
import { useEffect } from 'react';
import { auth } from './config/firebase';
import { useStateValue } from './config/StateProvider';
import Payment from './App/checkout/Payment';
import { loadStripe } from '@stripe/stripe-js'
import { Elements } from '@stripe/react-stripe-js'

const promise = loadStripe('pk_test_51Hs3SICtfyRxt3tdjf8cP1yu7pDihP302ZG0yMivXidw5VcYA3B0oUYHjRfzu5bIH7nHGWjENyjO9F9hCkuON9oA00XfiCl2kF');

// Important note! for responsiveness and consistency add to the home image: min-height: 500px; object-fit: cover. thank me later. 

function App() {
  const [{}, dispatch] = useStateValue();

  useEffect(() => {
    auth.onAuthStateChanged(authUser => {
      console.log('USER >>>> ', authUser);

      if(authUser) {
        dispatch({
          type: 'SET_USER',
          payload: authUser
        })
      } else {
        dispatch({
          type: 'SET_USER',
          payload: null
        })
      }
    })
  }, [])
  return (
    <Router>
      <div className="app">
        <Switch>
          <Route path="/login">
            <Login />
          </Route>
          <Route path="/payment">
            <Header />
            <Elements stripe={promise}>
              <Payment />
            </Elements>
          </Route>
          <Route path="/checkout">
            <Header />
            <Checkout />
          </Route>
          <Route path="/" exact>
            <Header />
            <Home />
          </Route>
        </Switch>
      </div>
    </Router>
  );
}

export default App;
