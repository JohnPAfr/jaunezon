import firebase from 'firebase'

const firebaseConfig = {
  apiKey: "AIzaSyAyOCvK9st8V1rR_AQK5nyIuSkHX8dy3Ek",
  authDomain: "jaunezon.firebaseapp.com",
  databaseURL: "https://jaunezon.firebaseio.com",
  projectId: "jaunezon",
  storageBucket: "jaunezon.appspot.com",
  messagingSenderId: "231815060765",
  appId: "1:231815060765:web:a1c71b20b0b0b12ceb55f4"
};

const firebaseApp = firebase.initializeApp(firebaseConfig);

const db = firebaseApp.firestore();
const auth = firebase.auth();

export { db, auth };