export const initialState = {
  basket: [],
  user: null
};

export const getBasketTotal = (basket) =>
    basket?.reduce((amount, item) => amount + item.price, 0)

const reducer = (state, action) => {

  console.log(action);
  
  switch(action.type) {
    case 'ADD_TO_BASKET':
        return {...state, basket: [...state.basket, action.payload]};
    case 'DELETE_FROM_BASKET':
      const index = state.basket.findIndex(
        (basketItem) => basketItem.id === action.payload
      );
      let newBasket = [...state.basket];
      if(index >= 0) {
        newBasket.splice(index, 1)
      } else {
        console.warn("Can't remove product")
      }
      return {
        ...state,
        basket: newBasket
      }
    case 'SET_USER':
      return {...state, user: action.payload}
    default:
      console.log('Unknown action type', action.type);
  }
};

export default reducer;