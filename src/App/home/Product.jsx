import React from 'react';
import { useStateValue } from '../../config/StateProvider'

function Product({ id, title, price, rating, image,  }) {
  const [{ basket }, dispatch] = useStateValue();

  const addToBasket = () => {
    dispatch({
      type: 'ADD_TO_BASKET',
      payload: {
        id: id,
        title: title,
        image: image,
        price: price,
        rating: rating
    }})
  }

  return (
    <div className="product">
      <div className="product__info">
        <p>{title}</p>
        <p className="product__price">
          <small>$</small>
          <strong>{price}</strong>
        </p>
        <div className="product__stars">
        {Array(rating).fill().map((_, i) => {
          return <p key={i}>🌟</p>
        })}
        </div>
      </div>
      <img 
      className="product__image"
      src={image}
      alt={title}/>
      <button className="product__button" onClick={addToBasket}>Add to basket</button>
      
    </div>
  )
}

export default Product
