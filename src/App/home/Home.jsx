import React from 'react';
import Banner from './Banner';
import './Home.css';
import Product from './Product';

function Home() {
  return (
    <div className="home">
      <div className="home__container">
        <Banner />

        <div className="home__row">
          <Product 
            id="45587429"
            title="The Lean Startup"
            price={11.99}
            rating={4}
            image="https://images-na.ssl-images-amazon.com/images/I/81jgCiNJPUL.jpg"/>
          <Product 
            id="445678429"
            title="The Silent Patient: The Richard and Judy bookclub pick and Sunday Times Bestseller"
            price={9.99}
            rating={5}
            image="https://m.media-amazon.com/images/I/51E+5yLuLAL.jpg"/>
        </div>
        <div className="home__row">
          <Product 
            id="4963729"
            title="Xiaomi Redmi Note 9 Pro Smartphone 6GB RAM 64GB ROM 6.67"
            price={199.99}
            rating={3}
            image="https://images-na.ssl-images-amazon.com/images/I/61JwC4MGfGL._AC_SL1000_.jpg"/>
          <Product 
            id="145987429"
            title="Amazon Echo (3rd Gen) | Smart speaker with Alexa, Charcoal Fabric"
            price={98.99}
            rating={5}
            image="https://media.very.co.uk/i/very/P6LTG_SQ1_0000000071_CHARCOAL_SLf?$300x400_retinamobilex2$"/>
          <Product 
            id="0963689"
            title="ASUS FX571GT-AL717T PC Portable Gaming 15'' FHD 120HZ"
            price={799.99}
            rating={4}
            image="https://images-na.ssl-images-amazon.com/images/I/81OCpc9MkZL._AC_SL1500_.jpg"/>
        </div>
        <div className="home__row">
          <Product 
            id="8764590"
            title="LG TV OLED OLED55CX6"
            price={1500}
            rating={5}
            image="https://images-na.ssl-images-amazon.com/images/I/712P5w%2BsgsL._AC_SL1500_.jpg"/>
        </div>


      </div>
    </div>
  )
}

export default Home
