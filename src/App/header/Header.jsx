import React from 'react';
import HeaderNav from './HeaderNav';
import SearchBar from './SearchBar';
import "./Header.css";
import { Link } from 'react-router-dom';

function Header() {
  return (
    <div className="header">
      <Link to="/">
        <img 
        className="header__logo"
        src="http://pngimg.com/uploads/amazon/amazon_PNG11.png" alt="amazon logo"/>
      </Link>
      <SearchBar />
      <HeaderNav />
    </div>
  )
}

export default Header
