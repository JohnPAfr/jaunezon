import React from 'react'
import ShoppingBasketIcon from "@material-ui/icons/ShoppingBasket"
import { Link } from 'react-router-dom'
import { useStateValue } from '../../config/StateProvider'
import { auth } from '../../config/firebase';

function HeaderNav() {
  const [state, dispatch] = useStateValue();
  
  const handleAuthentication = () => {
    if(state.user) {
      auth.signOut();
    }
  }

  return (
    <div className="header__nav">
      <Link to={!state.user ? '/login' : '/'}>
        <div onClick={handleAuthentication} className="header__option">
          <span className="header__optionLineOne">
            Hello {state.user && state.user.email}
          </span>
          <span className="header__optionLineTwo">
            { state.user ? 'Sign Out' : 'Sign In'}
          </span>
        </div>
      </Link>
      <div className="header__option">
        <span className="header__optionLineOne">
          Return
        </span>
        <span className="header__optionLineTwo">
          & Orders
        </span>
      </div>
      <div className="header__option">
        <span className="header__optionLineOne">
          Your
        </span>
        <span className="header__optionLineTwo">
          Prime
        </span>
      </div>
      <Link to="/checkout" className="header__optionBasket">
          <ShoppingBasketIcon />
          <span className="header__optionLineTwo header__basketCount">
            {state.basket?.length}
          </span>
      </Link>
    </div>
  )
}

export default HeaderNav
