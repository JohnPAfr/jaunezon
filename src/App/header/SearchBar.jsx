import React from 'react'
import SearchIcon from "@material-ui/icons/Search"

function SearchBar() {
  return (
    <div className="header__search">
      <input type="text" className="header__searchInput"/>
      <SearchIcon className="header__searchIcon"/>
      {/* <button className="header__searchButton"></button> */}
    </div>
  )
}

export default SearchBar
