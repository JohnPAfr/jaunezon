import React from 'react';
import CurrencyFormat from "react-currency-format";
import { useHistory } from 'react-router-dom';

function Subtotal({price, nbr}) {
  const history = useHistory();

  return (
    <div className="subtotal">
      <CurrencyFormat
      renderText={(value) => (
        <>
          <p>
            Subtotal ({nbr} items):
            <strong>{`${value}`}</strong>
          </p>
          <small className="subtotal__gift">
            <input type="checkbox"/> This order contains a gift
          </small>
        </>
      )}
      decimalScale={2}
      value={price}
      displayType={"text"}
      thousandSeparator={true}
      prefix={"$"}/>

      <button onClick={e => history.push('/payment')}>Proceed to Checkout</button>
    </div>
  )
}

export default Subtotal
