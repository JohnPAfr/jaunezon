import { CardElement, useElements, useStripe } from '@stripe/react-stripe-js';
import React, { useState } from 'react';
import { Link } from 'react-router-dom';
import { useStateValue } from '../../config/StateProvider';
import CheckoutProduct from './CheckoutProduct';
import './Payment.css';

function Payment() {
  const [{ basket, user }, dispatch] = useStateValue();
  const [error, setError] = useState(null);
  const [disable, setDisable] = useState(true);



  const stripe = useStripe();
  const elements = useElements();

  const handleSubmit = (e) => {
      e.preventDefault()
  }
  const handleChange = (e) => {
      e.preventDefault();
      setDisable(e.empty);
      setError(e.error ? e.error.message : "");
  }

  return (
    <div className="payment">
      <div className="payment__container">
        <h1>Checkout (<Link to="/checkout">{basket?.length} items</Link>) </h1>
        <div className="payment__section">
          <div className="payment__title">
            <h3>Delivery Address</h3>
          </div>
          <div className="payment__address">
            <p>{user?.email}</p>
            <p>123 React Lane</p>
            <p>Los Angeles, CA</p>
          </div>
        </div>
        <div className="payment__section">
          <div className="payment__title">
            <h3>Review items and delivery</h3>
          </div>
          <div className="payment__items">
            {basket.map(item => {
              return <CheckoutProduct
                key={item.id}
                id={item.id}
                title={item.title}
                price={item.price}
                rating={item.rating}
                image={item.image}
              />
            })}
          </div>
        </div>
        <div className="payment__section">
          <div className="payment__title">
            <h3>Payment Method</h3>
          </div>
          <div className="payment__details">
              <form onSubmit={handleSubmit}>
                <CardElement onChange={handleChange}/>
              </form>
          </div>
        </div>
      </div>
    </div>
  )
}

export default Payment
