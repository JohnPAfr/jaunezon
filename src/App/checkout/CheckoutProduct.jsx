import React from 'react';
import { useStateValue } from '../../config/StateProvider'

function CheckoutProduct({ id, title, price, rating, image }) {
  const [{ basket }, dispatch] = useStateValue();

  const removeFromBasket = () => {
    dispatch({
      type: 'DELETE_FROM_BASKET',
      payload: id
    })
  }

  return (
    <div className="checkoutProduct">
      <img 
      className="checkoutProduct__image"
      src={image}
      alt={title}/>

      <div className="checkoutProduct__info">
        <p className="checkoutProduct__title">{title}</p>
        <p className="checkoutProduct__price">
          <small>$</small>
          <strong>{price}</strong>
        </p>
        <div className="checkoutProduct__stars">
        {Array(rating).fill().map((_, i) => {
          return <p key={i}>🌟</p>
        })}
        </div>
        <button className="checkoutProduct__button" onClick={removeFromBasket}>Remove from basket</button>
      </div>
    </div>
  )
}

export default CheckoutProduct
