import React from 'react';
import { getBasketTotal } from '../../config/reducer';
import { useStateValue } from '../../config/StateProvider';
import './Checkout.css';
import CheckoutProduct from './CheckoutProduct';
import Subtotal from './Subtotal';

function Checkout() {
  const [{ basket, user }, dispatch] = useStateValue();

  return (
    <div className="checkout">
      <div className="checkout__left">
        <img 
        src="https://images-na.ssl-images-amazon.com/images/G/02/UK_CCMP/TM/OCC_Amazon1._CB423492668_.jpg" 
        alt="amazon exclusive" 
        className="checkout__ad"/>
        <div>
          {user?.email && <h3>hello {user.email}</h3>}
          <h2 className="checkout__title">
            Your shopping Basket
          </h2>
          <div className="checkout__products">
            {basket.map(item => (
              <CheckoutProduct
                key={item.id}
                id={item.id}
                title={item.title}
                price={item.price}
                rating={item.rating}
                image={item.image}
              />
            ))}
          </div>
        </div>
      </div>
      <div className="checkout__right">
        <Subtotal price={getBasketTotal(basket)} nbr={basket.length}/>
      </div>
    </div>
  )
}

export default Checkout
